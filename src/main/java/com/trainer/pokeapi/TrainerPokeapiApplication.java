package com.trainer.pokeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainerPokeapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainerPokeapiApplication.class, args);
	}

}
